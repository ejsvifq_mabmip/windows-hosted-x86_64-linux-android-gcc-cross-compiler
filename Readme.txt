This is a canadian x86_64 android cross compiler for windows (multilibs). Built by the fast_io library's author

Unix Timestamp:1653808941.784862688
UTC:2022-05-29T07:22:21.784862688Z

fast_io:
https://github.com/cppfastio/fast_io

build	:	x86_64-linux-gnu
host	:	x86_64-w64-mingw32
target	:	x86_64-linux-android


NOTICE for clang:
compiler-rt provides compiler runtime for clang 15. If you want to use this as a sysroot for clang, you need to copy compiler-rt to clang's installation directory manually
